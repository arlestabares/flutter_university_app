import 'package:flutter/material.dart';
import 'package:flutter_universities_app/features/blocs/multi_bloc_provider.dart';
import 'package:flutter_universities_app/features/repositories/multi_repository_provider.dart';
import 'package:flutter_universities_app/routes/routes.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProviderWidget(
      child: MultiBlocProviderWidget(
        child: MaterialApp(
          theme: ThemeData(useMaterial3: true),
          title: 'Material App',
          // initialRoute: 'university_page',
          initialRoute: 'home_page',
          routes: appRoutes,
        ),
      ),
    );
  }
}
