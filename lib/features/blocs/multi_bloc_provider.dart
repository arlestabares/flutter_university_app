import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/data/repositories/university_repository.dart';
import 'package:flutter_universities_app/features/universities/presentation/bloc/universities_bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<UniversitiesBloc>(
        create: ((context) => UniversitiesBloc(
              repositoryImpl:
                  RepositoryProvider.of<UniversityRepositoryImpl>(context),
            )..add(GetDataEvent())),
      )
    ], child: child);
  }
}
