import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../universities/data/repositories/university_repository.dart';

class MultiRepositoryProviderWidget extends StatelessWidget {
  const MultiRepositoryProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(providers: [
      RepositoryProvider(create: ((context) => UniversityRepositoryImpl()))
    ], child: child);
  }
}
