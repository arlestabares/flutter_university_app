import 'dart:convert';

import 'package:flutter_universities_app/features/universities/data/credentials_api.dart';
import 'package:flutter_universities_app/features/universities/data/models/universities.dart';
import 'package:http/http.dart' as http;

abstract class UniversityRepository {
  Future getData();
}

class UniversityRepositoryImpl implements UniversityRepository {
  @override
  Future<List<University>> getData() async {
    final uri = Uri.https(CredentialsAPI.url, '/FE-Engineer-test/universities.json');
    final response = await _processData(uri);
    return response;
  }

  Future<List<University>> _processData(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final universityList = UniversitiesList.fromJsn(decodeData);
      return universityList.items;
    } catch (e) {
      return List.empty();
    }
  }
}
