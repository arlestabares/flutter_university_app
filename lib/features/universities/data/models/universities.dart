class UniversitiesList {
  List<University> items = [];
  UniversitiesList.fromJsn(List<dynamic> jsonList) {
    for (var element in jsonList) {
      final university = University.fromJson(element);
      items.add(university);
    }
  }
}

class University {
  University({
    this.alphaTwoCode,
    this.domains,
    this.country,
    this.stateProvince,
    this.webPages,
    this.name,
  });

  AlphaTwoCode? alphaTwoCode;
  List<String>? domains;
  Country? country;
  String? stateProvince;
  List<String>? webPages;
  String? name;

  factory University.fromJson(Map<String, dynamic> json) => University(
        alphaTwoCode: alphaTwoCodeValues.map![json["alpha_two_code"]],
        domains: List<String>.from(json["domains"].map((x) => x)),
        country: countryValues.map![json["country"]],
        stateProvince: json["state-province"] ?? '',
        webPages: List<String>.from(json["web_pages"].map((x) => x)),
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "alpha_two_code": alphaTwoCodeValues.reverse[alphaTwoCode],
        "domains": List<dynamic>.from(domains!.map((x) => x)),
        "country": countryValues.reverse[country],
        "state-province": stateProvince,
        "web_pages": List<dynamic>.from(webPages!.map((x) => x)),
        "name": name,
      };
}

enum AlphaTwoCode { US }

final alphaTwoCodeValues = EnumValues({"US": AlphaTwoCode.US});

enum Country { UNITED_STATES }

final countryValues = EnumValues({"United States": Country.UNITED_STATES});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap ??= map!.map((k, v) => MapEntry(v, k));
    return reverseMap!;
  }
}
