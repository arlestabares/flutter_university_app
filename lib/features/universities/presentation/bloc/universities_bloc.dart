import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/data/models/universities.dart';
import 'package:flutter_universities_app/features/universities/data/repositories/university_repository.dart';

part 'universities_event.dart';
part 'universities_state.dart';

class UniversitiesBloc extends Bloc<UniversitiesEvent, UniversitiesState> {
  final UniversityRepositoryImpl repositoryImpl;
  UniversitiesBloc({required this.repositoryImpl}) : super(initialState) {
    on<GetDataEvent>(_onGetdata);
    on<ShowListViewEvent>(_onShowListViewEvent);
    on<ShowDetailsListViewEvent>(_onShowDetailsListView);
    on<ShowGridViewEvent>(_onShowGridViewEvent);
    on<ShowDetailsGridViewViewEvent>(_onShowDetailsGridView);
    on<ShowImageListViewEvent>(_onShowImageListView);
    on<ShowImageGridViewViewEvent>(_onShowImageGridView);
  }
  static UniversitiesState get initialState =>
      const UniversitiesInitial(Model());

  _onGetdata(GetDataEvent event, Emitter<UniversitiesState> emit) async {
    final response = await repositoryImpl.getData();
    emit(GetDataState(state.model.copyWith(universitiesList: response)));
  }

  _onShowListViewEvent(
      ShowListViewEvent event, Emitter<UniversitiesState> emit) async {
    emit(
      ShowListViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList),
      ),
    );
  }

  _onShowGridViewEvent(
      ShowGridViewEvent event, Emitter<UniversitiesState> emit) async {
    emit(
      ShowGridViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList),
      ),
    );
  }

  _onShowDetailsListView(
      ShowDetailsListViewEvent event, Emitter<UniversitiesState> emit) async {
    emit(ShowDetailsListViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList)));
  }

  _onShowDetailsGridView(ShowDetailsGridViewViewEvent event,
      Emitter<UniversitiesState> emit) async {
    emit(ShowDetailsGridViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList)));
  }

  _onShowImageListView(
      ShowImageListViewEvent event, Emitter<UniversitiesState> emit) async {
    emit(ShowImageListViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList)));
  }

  _onShowImageGridView(
      ShowImageGridViewViewEvent event, Emitter<UniversitiesState> emit) async {
    emit(ShowImageGridViewState(
        state.model.copyWith(universitiesList: state.model.universitiesList)));
  }
}
