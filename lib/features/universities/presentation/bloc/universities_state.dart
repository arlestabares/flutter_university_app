part of 'universities_bloc.dart';

abstract class UniversitiesState extends Equatable {
  const UniversitiesState(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class UniversitiesInitial extends UniversitiesState {
  const UniversitiesInitial(Model model) : super(model);
}

class GetDataState extends UniversitiesState {
  const GetDataState(Model model) : super(model);
}

class GetDataErrorState extends UniversitiesState {
  const GetDataErrorState(Model model) : super(model);
}

class ShowDetailsListViewState extends UniversitiesState {
  const ShowDetailsListViewState(Model model) : super(model);
}

class ShowImageListViewState extends UniversitiesState {
  const ShowImageListViewState(Model model) : super(model);
}

class ShowDetailsGridViewState extends UniversitiesState {
  const ShowDetailsGridViewState(Model model) : super(model);
}

class ShowImageGridViewState extends UniversitiesState {
  const ShowImageGridViewState(Model model) : super(model);
}

class ShowDetailsErrorState extends UniversitiesState {
  const ShowDetailsErrorState(Model model) : super(model);
}

class ShowListViewState extends UniversitiesState {
  const ShowListViewState(Model model) : super(model);
}

class ShowGridViewState extends UniversitiesState {
  const ShowGridViewState(Model model) : super(model);
}

class Model extends Equatable {
  final bool isListView;
  final List<University> universitiesList;

  const Model({
    this.isListView = false,
    this.universitiesList = const <University>[],
  });

  Model copyWith({
    bool? isListView,
    List<University>? universitiesList,
  }) =>
      Model(
        isListView: isListView ?? this.isListView,
        universitiesList: universitiesList ?? this.universitiesList,
      );

  @override
  List<Object?> get props => [universitiesList, isListView];
}
