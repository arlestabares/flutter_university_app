part of 'universities_bloc.dart';

abstract class UniversitiesEvent extends Equatable {
  const UniversitiesEvent();

  @override
  List<Object> get props => [];
}

class GetDataEvent extends UniversitiesEvent {}

class ShowListViewEvent extends UniversitiesEvent {}

class ShowGridViewEvent extends UniversitiesEvent {}

class ShowDetailsListViewEvent extends UniversitiesEvent {}

class ShowImageListViewEvent extends UniversitiesEvent {}

class ShowDetailsGridViewViewEvent extends UniversitiesEvent {}

class ShowImageGridViewViewEvent extends UniversitiesEvent {}
