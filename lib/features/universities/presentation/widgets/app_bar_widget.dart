import 'package:flutter/material.dart';

AppBar appbarWidget(BuildContext context) {
  return AppBar(
    title: const Text('ShowImageListView'),
    elevation: 0.0,
    backgroundColor: Colors.white,
    leading: IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(
        Icons.arrow_back_ios,
        color: Colors.black,
      ),
    ),
  );
}
