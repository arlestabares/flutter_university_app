import 'package:flutter/material.dart';

class SizeTransitionRoute extends PageRouteBuilder {
  final Widget? page;
  SizeTransitionRoute({this.page})
      : super(
          pageBuilder: (context, animation, secondaryAnimation) => page!,
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              Align(
            child: SizeTransition(
              sizeFactor: animation,
              child: child,
            ),
          ),
        );
}
