import 'package:flutter/material.dart';
import 'package:flutter_universities_app/features/universities/data/models/universities.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/text_widget.dart';

class GridViewCard extends StatelessWidget {
  const GridViewCard({
    Key? key,
    required this.university,
    required this.domains,
    required this.webPages,
  }) : super(key: key);
  final University university;
  final String domains;
  final String webPages;
  @override
  Widget build(BuildContext context) {
    // int count = 0;
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 8.0),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextWidget(
                  text: '${university.alphaTwoCode!}',
                  style: const TextStyle(fontSize: 21.0),
                ),
                TextWidget(
                  text: '${university.country!}',
                  style: const TextStyle(fontSize: 21.0),
                ),
                TextWidget(
                  text: university.name!,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                TextWidget(
                    text: domains, style: const TextStyle(fontSize: 16.0)),
                TextWidget(
                    text: webPages, style: const TextStyle(fontSize: 18.0)),
              ],
            ),
          ),
          const SizedBox(height: 12.0),
        ],
      ),
    );
  }
}
