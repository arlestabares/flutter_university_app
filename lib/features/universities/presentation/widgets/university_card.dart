import 'package:flutter/material.dart';
import 'package:flutter_universities_app/features/universities/data/models/universities.dart';

class UniversityCard extends StatelessWidget {
  final University university;
  final String domainName;
  final String webPages;
  const UniversityCard({
    Key? key,
    required this.university,
    required this.domainName,
    required this.webPages,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 15.0,
            offset: Offset(-3.0, 10.0),
          )
        ],
        shape: BoxShape.rectangle,
        color: Colors.transparent,
      ),
      margin: const EdgeInsets.fromLTRB(2.0, 8, 5.0, 5.0),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
          bottomLeft: Radius.circular(30.0),
          bottomRight: Radius.circular(30.0)
        ),
        child: Container(
          color: Colors.white,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Hero(
              //   tag: university.name.toString(),
              //   child: Image.network(
              //     '${university.name}',
              //     width: 150,
              //     height: 160,
              //     fit: BoxFit.fill,
              //   ),
              // ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        university.name!,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 20.0),
                      ),
                      Text(
                        university.name!,
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                      Text(
                        domainName,
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                      Text(
                        webPages,
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
