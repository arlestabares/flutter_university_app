import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  const TextWidget(
      {Key? key,
      required this.text,
      this.fontSize,
      this.style,
      this.overflow,
      this.maxLines})
      : super(key: key);
  final String? text;
  final double? fontSize;
  final TextStyle? style;
  final TextOverflow? overflow;
  final int? maxLines;
  @override
  Widget build(BuildContext context) {
    return Text(
      text!,
      overflow: overflow,
      maxLines: maxLines,
      style: style ??
          Theme.of(context)
              .textTheme
              .headline6!
              .copyWith(fontSize: fontSize, fontWeight: FontWeight.bold),
    );
  }
}
