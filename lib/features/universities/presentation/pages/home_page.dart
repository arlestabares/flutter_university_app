import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/bloc/universities_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/views/university_gridview.dart';
import 'package:flutter_universities_app/features/universities/presentation/views/university_listview.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isGridView = false;
  bool isListView = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Universities',
          style: TextStyle(
            color: Colors.black.withOpacity(0.8),
          ),
        ),
      ),
      body: listViewAndGridViewContainer(context),
    );
  }

  Container listViewAndGridViewContainer(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 21.0),
      child: Column(
        children: <Widget>[
          Container(
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              border: Border.all(color: Colors.green),
            ),
            child: Row(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    context.read<UniversitiesBloc>().add(ShowListViewEvent());
                    setState(() {
                      isListView = true;
                      isGridView = false;
                    });
                  },
                  child: Container(
                    width: 200,
                    decoration: BoxDecoration(
                      color: Colors.green[700]!.withOpacity(0.9),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(12.0),
                        bottomLeft: Radius.circular(12.0),
                      ),
                    ),
                    child: const Center(
                      child: Text(
                        'Show ListView',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      context.read<UniversitiesBloc>().add(ShowGridViewEvent());
                      setState(() {
                        isGridView = true;
                        isListView = false;
                      });
                    },
                    child: Center(
                      child: Text(
                        'Show GridView',
                        style:
                            TextStyle(fontSize: 18.0, color: Colors.green[700]),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          isListView
              ? const Expanded(child: UniversityListView())
              : const Expanded(child: UniversityGridView()),
        ],
      ),
    );
  }
}
