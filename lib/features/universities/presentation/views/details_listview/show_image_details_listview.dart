import 'dart:io';
import 'dart:async';
// import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/data/models/universities.dart';
import 'package:flutter_universities_app/features/universities/presentation/bloc/universities_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/app_bar_widget.dart';
import 'package:image_picker/image_picker.dart';

class ShowImageDetailsListView extends StatelessWidget {
  const ShowImageDetailsListView(
      {Key? key,
      required this.university,
      required this.domainName,
      required this.webPages})
      : super(key: key);
  final University university;
  final String domainName;
  final String webPages;

  Future seleccionImage(op) async {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(context),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(2.0),
          child: Column(
            children: [
              ShowImageDetails(
                university: university,
                domainName: domainName,
                webPages: webPages,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShowImageDetails extends StatefulWidget {
  const ShowImageDetails(
      {Key? key,
      required this.university,
      required this.domainName,
      required this.webPages})
      : super(key: key);
  final University university;
  final String domainName;
  final String webPages;

  @override
  State<ShowImageDetails> createState() => _ShowImageDetailsState();
}

class _ShowImageDetailsState extends State<ShowImageDetails> {
  bool isImageLoad = false;
  File? image;
  final picker = ImagePicker();
  Future selectedImage(op) async {
    var pickedFile; //sera el archivo seleccionado
    if (op == 1) {
      pickedFile = await picker.pickImage(source: ImageSource.camera);
    } else {
      pickedFile = await picker.pickImage(source: ImageSource.gallery);
    }
    setState(() {
      if (pickedFile != null) {
        image = File(pickedFile.path);
        isImageLoad = true;
      } else {}
    });
  }

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold);
    return BlocBuilder<UniversitiesBloc, UniversitiesState>(
      builder: (context, state) {
        // if (state is ShowImageListViewState) {
        return Container(
          padding: const EdgeInsets.all(12.0),
          // height: 620.0,
          child: Column(
            children: [
              Stack(
                children: [
                  // UserImage(),
                  BlocBuilder<UniversitiesBloc, UniversitiesState>(
                    builder: (context, state) {
                      // if (state is ShowImageListViewState) {
                      return !isImageLoad
                          ? Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image:
                                      AssetImage('assets/images/no-image.jpg'),
                                ),
                              ),
                              height: 250.0,
                            )
                          : Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image: FileImage(image!),
                                ),
                              ),
                              height: 250.0,
                            );
                      // }
                      // return const Center(child: CircularProgressIndicator());
                    },
                  ),
                  // CamaraOption(),
                  Positioned(
                    right: 100.0,
                    top: 200.0,
                    child: CircleAvatar(
                      radius: 25.0,
                      child: IconButton(
                        onPressed: () {
                          showModalBottomSheet<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0),
                                height: 230,
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: const [
                                          Text('Foto del perfil')
                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(12.0),
                                                child: CircleAvatar(
                                                  radius: 25.0,
                                                  child: IconButton(
                                                    onPressed: () {
                                                      setState(() {});
                                                      selectedImage(1);
                                                    },
                                                    icon: const Icon(Icons
                                                        .camera_alt_outlined),
                                                  ),
                                                ),
                                              ),
                                              const Text("Camara")
                                            ],
                                          ),
                                          const SizedBox(width: 21.0),
                                          Column(
                                            children: [
                                              Container(
                                                padding: const EdgeInsets.all(
                                                  12.0,
                                                ),
                                                child: CircleAvatar(
                                                  radius: 25.0,
                                                  child: IconButton(
                                                    onPressed: () {
                                                      setState(() {});
                                                      selectedImage(2);
                                                      // image == null
                                                      //     ? const Center()
                                                      //     : Image.file(
                                                      //         image!,
                                                      //       );
                                                    },
                                                    icon: const Icon(
                                                      Icons.image,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const Text("Galeria")
                                            ],
                                          ),
                                        ],
                                      ),
                                      ElevatedButton(
                                        child: const Text('Close'),
                                        onPressed: () => Navigator.pop(context),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                        icon: const Icon(Icons.camera_alt_outlined),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 41.0),
              Column(
                children: [
                  Text(
                    widget.university.name!,
                    style: const TextStyle(
                      fontSize: 21.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  
                  Text(
                    widget.university.name!,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16.0,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                  Text(
                    widget.domainName,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16.0,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                  Text(
                    widget.webPages,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 16.0,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                  const SizedBox(height: 8.0),
                 
                ],
              ),
            ],
          ),
        );
        // }
        // return const Center(child: CircularProgressIndicator());
      },
    );
  }
}
