// import 'package:flutter/material.dart';
// import 'package:flutter_universities_app/features/universities/data/models/universities.dart';

// class DetailsListView extends StatelessWidget {
//   const DetailsListView(
//       {Key? key,
//       required this.university,
//       required this.domainName,
//       required this.webPages})
//       : super(key: key);
//   final University university;
//   final String domainName;
//   final String webPages;

//   @override
//   Widget build(BuildContext context) {
//     // final size = MediaQuery.of(context).size;
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           '${university.name}',
//           maxLines: 1,
//           style: const TextStyle(fontSize: 16.0),
//           overflow: TextOverflow.ellipsis,
//         ),
//         leading: IconButton(
//           icon: const Icon(Icons.arrow_back_ios_new),
//           onPressed: () => Navigator.pop(context),
//         ),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 12.0),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             // Container(
//             //   height: size.height * 0.3,
//             //   width: size.width,
//             //   // color: Colors.black38,
//             //   padding: const EdgeInsets.all(5.0),
//             //   child: HeroWidget(
//             //     tag: 'hero',
//             //     radius: 51.0,
//             //     assetImage: 'assets/images/no-image.jpg',
//             //     networkImage: "${university.name}",
//             //     fit: BoxFit.cover,
//             //   ),
//             // ),
//             const SizedBox(height: 22.0),
//             Text(
//               university.name!,
//               style: const TextStyle(
//                 fontSize: 21.0,
//                 fontWeight: FontWeight.bold,
//               ),
//             ),
//             const SizedBox(height: 12.0),
//             Row(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Expanded(
//                   child: Text(
//                     university.name!.isEmpty
//                         ? 'No description......'
//                         : university.name!,
//                     style: TextStyle(
//                         fontSize: university.name!.isEmpty ? 21.0 : 16.0,
//                         fontWeight: FontWeight.bold),
//                   ),
//                 ),
//               ],
//             ),
//             const SizedBox(height: 8.0),
//             Text(
//               '${university.name}',
//               style: const TextStyle(fontSize: 18.0),
//             ),
//             Text(
//               '${university.name}',
//               style: const TextStyle(fontSize: 18.0),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
