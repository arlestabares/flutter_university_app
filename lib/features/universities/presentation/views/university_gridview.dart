import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/bloc/universities_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/views/details_gridview/details_grid_view.dart';
import 'package:flutter_universities_app/features/universities/presentation/views/details_gridview/show_image_details_gridview.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/university_card.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/size_transition.dart';

class UniversityGridView extends StatefulWidget {
  const UniversityGridView({Key? key}) : super(key: key);

  @override
  State<UniversityGridView> createState() => _UniversityGridViewState();
}

class _UniversityGridViewState extends State<UniversityGridView> {
  ContainerTransitionType transitionType = ContainerTransitionType.fade;
  final _scrollController = ScrollController();
  int page = 1;
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollListener)
      ..dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.position.maxScrollExtent ==
        _scrollController.offset) {
      page++;
      setState(() {});
      context.read<UniversitiesBloc>().add(GetDataEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocBuilder<UniversitiesBloc, UniversitiesState>(
      buildWhen: (_, state) => state is ShowDetailsListViewState,
      builder: (context, state) {
        // switch (state.model.status) {
        // case BookSearchStatus.success:
        // if (state.model.bookEntityList!.isEmpty) {
        //   return const Center(
        //     key: Key('No Book'),
        //     child: Card(
        //       child: ListTile(
        //         title: Text('There are not books to show'),
        //       ),
        //     ),
        //   );
        // }

        return SizedBox(
          // height: size.width > 400 ? size.height * 0.2 : size.height * 0.2,
          child: BlocBuilder<UniversitiesBloc, UniversitiesState>(
            buildWhen: (_, state) => state is ShowGridViewState,
            builder: (BuildContext context, state) {
              if (state is ShowGridViewState) {
                return GridView.builder(
                  shrinkWrap: true,
                  itemCount: state.model.universitiesList.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: size.width > 400 ? 0.8 : 0.6
                      // childAspectRatio: MediaQuery.of(context).size.width /
                      //     (MediaQuery.of(context).size.height / 0.9),
                      ),
                  itemBuilder: (context, int index) {
                    final university = state.model.universitiesList[index];
                    final domain =
                        state.model.universitiesList[index].domains![0];
                    final webPages =
                        state.model.universitiesList[index].webPages![0];
                    return InkWell(
                      onTap: () => Navigator.push(
                        context,
                        SizeTransitionRoute(
                          page: ShowImageDetailsGridView(
                            university: university,
                            domainName: domain,
                            webPages: webPages,
                          ),
                        ),
                      ),
                      child: UniversityCard(
                        university: university,
                        domainName: domain,
                        webPages: webPages,
                      ),
                    );
                  },
                );
              }
              return const SizedBox.shrink();
            },
          ),
        );
      },
    );
  }
}
