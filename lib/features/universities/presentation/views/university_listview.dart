import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/bloc/universities_bloc.dart';
import 'package:flutter_universities_app/features/universities/presentation/views/details_listview/show_image_details_listview.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/size_transition.dart';
import 'package:flutter_universities_app/features/universities/presentation/widgets/university_card.dart';

class UniversityListView extends StatefulWidget {
  const UniversityListView({Key? key}) : super(key: key);

  @override
  State<UniversityListView> createState() => _UniversityListViewState();
}

class _UniversityListViewState extends State<UniversityListView> {
  ContainerTransitionType transitionType = ContainerTransitionType.fade;
  final _scrollController = ScrollController();
  int page = 1;
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollListener)
      ..dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.position.maxScrollExtent ==
        _scrollController.offset) {
      page++;
      setState(() {});
      context.read<UniversitiesBloc>().add(GetDataEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UniversitiesBloc, UniversitiesState>(
      buildWhen: (_, state) => state is ShowListViewState|| state is GetDataState,
      builder: (BuildContext context, state) {
        if (state is ShowListViewState||  state is GetDataState) {
          if (state.model.universitiesList.isEmpty) {
            return const Center(
              child: Text('No Universities added'),
            );
          }
          return ListView.builder(
            itemCount: state.model.universitiesList.length,
            itemBuilder: (context, index) {
              final university = state.model.universitiesList[index];
              final domain = state.model.universitiesList[index].domains![0];
              final webPages = state.model.universitiesList[index].webPages![0];
              return InkWell(
                onTap: () => Navigator.push(
                  context,
                  SizeTransitionRoute(
                    page: ShowImageDetailsListView(
                      university: university,
                      domainName: domain,
                      webPages: webPages,
                    ),
                  ),
                ),
                child: UniversityCard(
                  university: university,
                  domainName: domain,
                  webPages: webPages,
                ),
              );
            },
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
