import 'package:flutter/material.dart';
import 'package:flutter_universities_app/features/universities/presentation/pages/home_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home_page': (_) => const HomePage()
};
